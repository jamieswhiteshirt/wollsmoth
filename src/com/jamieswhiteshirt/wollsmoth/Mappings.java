package com.jamieswhiteshirt.wollsmoth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.objectweb.asm.tree.ClassNode;

public class Mappings
{
	public static HashMap<String, ClassMapping> classes = new HashMap<String, ClassMapping>();
	public static HashMap<String, PackageMapping> packages = new HashMap<String, PackageMapping>();
	
	public static String getPackage(String packageName)
	{
		StringBuilder sb = new StringBuilder();
		
		PackageMapping p = null;
		
		String[] subPackages = packageName.split("/");
		for(int i = 0; i < subPackages.length; i++)
		{
			if(i == 0)
			{
				p = packages.get(subPackages[i]);
			}
			else
			{
				if(p != null) p = p.getChild(subPackages[i]);
				sb.append("/");
			}
			
			if(p != null)
			{
				sb.append(p.name);
			}
			else
			{
				sb.append(subPackages[i]);
			}
		}
		
		return sb.toString();
	}
	
	public static String getClass(String className)
	{
		ClassMapping c = classes.get(className);
		if(c != null)
		{
			return c.name;
		}
		return className;
	}
	
	public static ClassMapping getClassMapping(String className)
	{
		return classes.get(className);
	}
	
	public static MethodMapping getMethod(String className, MethodMapping method)
	{
		ClassMapping c = classes.get(className);
		if(c != null)
		{
			MethodMapping m = c.getMethod(method);
			if(m != null)
			{
				return m;
			}
		}
		return new MethodMapping(method);
	}
	
	public static MethodMapping getMethod(String className, String methodName, String methodDesc)
	{
		return getMethod(className, new MethodMapping(methodName, methodDesc));
	}
	
	public static FieldMapping getField(String className, FieldMapping field)
	{
		ClassMapping c = classes.get(className);
		if(c != null)
		{
			FieldMapping f = c.getField(field);
			if(f != null)
			{
				return f;
			}
		}
		return new FieldMapping(field);
	}
	
	public static FieldMapping getField(String className, String fieldName, String fieldDesc)
	{
		return getField(className, new FieldMapping(fieldName, fieldDesc));
	}
	
	public static void map(ClassNode node, boolean readOnly)
	{
		classes.put(node.name, new ClassMapping(node, readOnly));
		
		if(!readOnly)
		{
			PackageMapping p = null;
			String[] subPackages = node.name.split("/");
			for(int i = 0; i < subPackages.length - 1; i++)
			{
				if(p == null)
				{
					p = packages.get(subPackages[i]);
					if(p == null)
					{
						p = new PackageMapping(subPackages[i]);
						packages.put(subPackages[i], p);
					}
				}
				else
				{
					p = p.getChild(subPackages[i]);
				}
			}
		}
	}
	
	public static void resolveInterfaces()
	{
		Iterator<ClassMapping> it = classes.values().iterator();
		while(it.hasNext())
		{
			ClassMapping c = it.next();
			for(int i = 0; i < c.interfaces.size(); i++)
			{
				ClassMapping c2 = classes.get(c.interfaces.get(i));
				if(c2 != null) c2.isInterface = true;
			}
		}
	}
	
	public static ArrayList<ClassMapping> directChildrenOf(String className)
	{
		ArrayList<ClassMapping> list = new ArrayList<ClassMapping>();
		
		Iterator<ClassMapping> it = classes.values().iterator();
		while(it.hasNext())
		{
			ClassMapping c = it.next();
			if(c.superClass.equals(className)) list.add(c);
		}
		
		return list;
	}
	
	public static ArrayList<ClassMapping> interfaces()
	{
		ArrayList<ClassMapping> list = new ArrayList<ClassMapping>();
		
		Iterator<ClassMapping> it = classes.values().iterator();
		while(it.hasNext())
		{
			ClassMapping c = it.next();
			if(c.isInterface) list.add(c);
		}
		
		return list;
	}
	
	public static ArrayList<ClassMapping> parentlessClasses()
	{
		ArrayList<ClassMapping> list = new ArrayList<ClassMapping>();
		
		Iterator<ClassMapping> it = classes.values().iterator();
		while(it.hasNext())
		{
			ClassMapping c = it.next();
			ClassMapping c2 = classes.get(c.superClass);
			if(c2 == null && !c.isInterface) list.add(c);
		}
		
		return list;
	}
	
	public static String formatDescriptor(String desc)
	{
		StringBuilder newDesc = new StringBuilder();
		
		int beginIndex = 0;
		int endIndex = 0;
		
		while(beginIndex < desc.length())
		{
			endIndex = desc.indexOf('L', beginIndex + 1);
			if(endIndex == -1) endIndex = beginIndex + 1;
			
			String toAppend = null;
			if(beginIndex < desc.length())
			{
				if(desc.charAt(beginIndex) == 'L')
				{
					endIndex = beginIndex;
					
					int nestingBegin = -1;
					int nestingEnd = 0;
					for(int nesting = 0; endIndex < desc.length(); endIndex++)
					{
						char c = desc.charAt(endIndex);
						if(c == '<')
						{
							if(nesting++ == 0)
							{
								nestingBegin = endIndex;
							}
						}
						else if(c == '>')
						{
							if(nesting-- == 1)
							{
								nestingEnd = endIndex;
							}
						}
						else if(c == ';' && nesting == 0)
						{
							break;
						}
					}
					
					if(nestingBegin != -1)
					{
						StringBuilder sb = new StringBuilder();
						sb.append("L");
						sb.append(Mappings.getClass(desc.substring(beginIndex + 1, nestingBegin + 1)));
						sb.append(formatDescriptor(desc.substring(nestingBegin + 1, nestingEnd)));
						sb.append(desc.substring(nestingEnd, endIndex));
						toAppend = sb.toString();
					}
					else
					{
						toAppend = "L" + Mappings.getClass(desc.substring(beginIndex + 1, endIndex));
					}
				}
				else
				{
					toAppend = desc.substring(beginIndex, endIndex);
				}
			}
			else
			{
				toAppend = "";
			}
			
			newDesc.append(toAppend);
			
			beginIndex = endIndex;
		}
		
		return newDesc.toString();
	}
}