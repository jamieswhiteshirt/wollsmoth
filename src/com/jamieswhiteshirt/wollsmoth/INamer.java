package com.jamieswhiteshirt.wollsmoth;

public interface INamer
{
	public String getClassName(int index);
	
	public String getClassFieldName(int index);
	
	public String getClassMethodName(int index);

	public String getInterfaceName(int index);
	
	public String getInterfaceFieldName(int index);
	
	public String getInterfaceMethodName(int index);
	
	public String getLocalVariableName(int index);
	
	public String getPackageName(int index);
}