package com.jamieswhiteshirt.wollsmoth;

import org.objectweb.asm.tree.FieldNode;

public class FieldMapping extends ValueMapping
{
	public FieldMapping(FieldNode node)
	{
		this.name = node.name;
		this.desc = node.desc;
	}
	
	public FieldMapping(String name, String desc)
	{
		this.name = name;
		this.desc = desc;
	}
	
	public FieldMapping(FieldMapping other)
	{
		this.name = other.name;
		this.desc = other.desc;
	}
}