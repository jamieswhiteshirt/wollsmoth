package com.jamieswhiteshirt.wollsmoth;

public class Oooh implements INamer
{
	private static String make(int length, boolean capitalized, char filler)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(capitalized ? Character.toUpperCase(filler) : filler);
		
		for(int i = 1; i <= length; i++)
		{
			sb.append(filler);
		}
		sb.append('h');
		
		return sb.toString();
	}
	
	private static String Oh(int length)
	{
		return make(length, true, 'o');
	}
	
	private static String oh(int length)
	{
		return make(length, false, 'o');
	}
	
	private static String Ah(int length)
	{
		return make(length, true, 'a');
	}
	
	private static String ah(int length)
	{
		return make(length, false, 'a');
	}
	
	private static String mh(int length)
	{
		return make(length, false, 'm');
	}
	
	private static String uh(int length)
	{
		StringBuilder sb = new StringBuilder();
		sb.append('u');
		
		for(int i = 0; i <= length; i++)
		{
			sb.append('h');
		}
		
		return sb.toString();
	}
	
	public String getClassName(int index)
	{
		return Oh(index);
	}

	public String getClassFieldName(int index)
	{
		return oh(index);
	}

	public String getClassMethodName(int index)
	{
		return oh(index);
	}

	public String getInterfaceName(int index)
	{
		return Ah(index);
	}

	public String getInterfaceFieldName(int index)
	{
		return ah(index);
	}

	public String getInterfaceMethodName(int index)
	{
		return ah(index);
	}
	
	public String getLocalVariableName(int index)
	{
		return mh(index);
	}
	
	public String getPackageName(int index)
	{
		return uh(index);
	}
}