package com.jamieswhiteshirt.wollsmoth;

import org.objectweb.asm.tree.MethodNode;

public class MethodMapping extends ValueMapping
{
	public MethodMapping(MethodNode node)
	{
		this.name = node.name;
		this.desc = node.desc;
	}
	
	public MethodMapping(String name, String desc)
	{
		this.name = name;
		this.desc = desc;
	}
	
	public MethodMapping(MethodMapping other)
	{
		this.name = other.name;
		this.desc = other.desc;
	}
}