package com.jamieswhiteshirt.wollsmoth;

import java.util.HashMap;

public class PackageMapping
{
	public String name;
	public final HashMap<String, PackageMapping> children = new HashMap<String, PackageMapping>();
	
	public PackageMapping(String name)
	{
		this.name = name;
	}
	
	public PackageMapping getChild(String name)
	{
		PackageMapping p = children.get(name);
		if(p == null)
		{
			p = new PackageMapping(name);
			children.put(name, p);
		}
		return p;
	}
}