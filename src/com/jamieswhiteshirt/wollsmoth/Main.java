package com.jamieswhiteshirt.wollsmoth;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

public class Main
{
	private static HashMap<String, ClassNode> modifiableClassNodes = new HashMap<String, ClassNode>();
	private static JarFile originalJar;
	
	public static void main(String[] args)
	{
		Queue<String> argQueue = new LinkedList<String>();
		for(int i = 0; i < args.length; i++)
		{
			argQueue.add(args[i]);
		}
		
		if(args.length == 0)
		{
			System.out.println("Invalid amount of arguments. Specify path to .jar to obfuscate.");
			System.out.println("Use '-l path' to specify a path to a library or a folder containing libraries.");
			return;
		}
		
		String inPath = argQueue.remove();
		File in = new File(inPath);
		
		if(!in.canRead())
		{
			System.out.print("Cannot read file " + inPath);
			return;
		}
		
		try
		{
			originalJar = new JarFile(in);
			modifiableClassNodes = readJarFile(originalJar, false);
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return;
		}
		
		while(argQueue.size() > 0)
		{
			String s = argQueue.remove();
			if(s.equals("-l"))
			{
				String path = argQueue.remove();
				readLibrary(new File(path));
			}
			else
			{
				System.out.println("Could not recognize command " + s);
			}
		}
		
		readLibrary(new File(System.getProperty("java.home") + "/lib/rt.jar"));
		
		obfuscate();
		
		try
		{
			int dotIndex = inPath.lastIndexOf('.');
			String outPath = inPath.substring(0, dotIndex) + "-obfuscated" + inPath.substring(dotIndex);
			File out = new File(outPath);
			
			out.createNewFile();
			
			if(!out.canWrite())
			{
				System.out.print("Cannot write file " + outPath);
				return;
			}
			
			writeJarFile(out);
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return;
		}
		
		System.out.print("Sucessfully obfuscated!");
	}
	
	private static HashMap<String, ClassNode> readJarFile(JarFile jar, boolean isExternal) throws IOException
	{
		HashMap<String, ClassNode> classNodes = new HashMap<String, ClassNode>();
		
		for(Enumeration<JarEntry> e = originalJar.entries(); e.hasMoreElements();)
		{
			JarEntry entry = e.nextElement();
			if(entry.getName().endsWith(".class"))
			{
				InputStream input = originalJar.getInputStream(entry);
				
				ClassNode classNode = new ClassNode();
				ClassReader reader = new ClassReader(input);
				reader.accept(classNode, 0);
				
				classNodes.put(entry.getName(), classNode);
				
				input.close();
			}
		}
		
		mapAllEntries(classNodes, isExternal);
		
		return classNodes;
	}
	
	private static void mapAllEntries(HashMap<String, ClassNode> classNodes, boolean readOnly)
	{
		Iterator<ClassNode> it = classNodes.values().iterator();
		while(it.hasNext())
		{
			Mappings.map(it.next(), readOnly);
		}
		
		Mappings.resolveInterfaces();
	}
	
	private static void readLibrary(File fileOrFolder)
	{
		if(!fileOrFolder.exists())
		{
			System.out.print("Could not read jar file or folder " + fileOrFolder.getAbsolutePath());
			return;
		}
		
		try
		{
			if(fileOrFolder.isFile())
			{
				if(!fileOrFolder.canRead())
				{
					System.out.print("Could not read jar file " + fileOrFolder.getAbsolutePath());
					return;
				}
				
				readJarFile(new JarFile(fileOrFolder), false);
			}
			else if(fileOrFolder.isDirectory())
			{
				File[] fileList = fileOrFolder.listFiles();
				for(int i = 0; i < fileList.length; i++)
				{
					readLibrary(fileList[i]);
				}
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private static void obfuscate()
	{
		Obfuscator.obfuscate(modifiableClassNodes, new Oooh());
	}
	
	private static void writeJarFile(File jar) throws IOException
	{
		JarOutputStream output = new JarOutputStream(new FileOutputStream(jar));
		
		for(Enumeration<JarEntry> e = originalJar.entries(); e.hasMoreElements();)
		{
			JarEntry entry = e.nextElement();
			
			ClassNode classNode = modifiableClassNodes.get(entry.getName());
			if(classNode != null)
			{
				writeClass(output, entry, classNode);
			}
			else
			{
				if(!entry.getName().equals("META-INF/MANIFEST.MF"))
				{
					writeFile(output, entry);
				}
				else
				{
					writeManifest(output, entry);
				}
			}
		}
		
		output.close();
	}
	
	private static void writeClass(JarOutputStream output, JarEntry entry, ClassNode classNode) throws IOException
	{
		JarEntry outEntry = new JarEntry(classNode.name + ".class");
		output.putNextEntry(outEntry);
		
		ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
		classNode.accept(writer);
		output.write(writer.toByteArray());
	}
	
	private static void writeFile(JarOutputStream output, JarEntry entry) throws IOException
	{
		output.putNextEntry(new JarEntry(entry));
		InputStream input = originalJar.getInputStream(entry);
		
		byte[] buffer = new byte[16384];
		int len;
		while((len = input.read(buffer)) != -1)
		{
			output.write(buffer, 0, len);
		}
		
		input.close();
	}
	
	private static void writeManifest(JarOutputStream output, JarEntry entry) throws IOException
	{
		final String mainClass = "Main-Class: ";

		output.putNextEntry(new JarEntry(entry.getName()));
		InputStream input = originalJar.getInputStream(entry);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		String line = null;
		
		while((line = reader.readLine()) != null)
		{
			if(line.startsWith(mainClass))
			{
				line = mainClass + Mappings.getClass(line.substring(mainClass.length()).trim().replace('.', '/')).replace('/', '.');
			}
			
			line += System.lineSeparator();
			
			output.write(line.getBytes());
		}
		
		input.close();
	}
}