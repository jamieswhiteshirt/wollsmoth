package com.jamieswhiteshirt.wollsmoth;

public abstract class ValueMapping
{
	public String name;
	public String desc;
	
	@Override
	public boolean equals(Object o)
	{
		if(o == null || !(o instanceof ValueMapping)) return false;
		ValueMapping v = (ValueMapping)o;
		return name.equals(v.name) && desc.equals(v.desc);
	}
	
	@Override
	public int hashCode()
	{
		return name.hashCode() + desc.hashCode();
	}
}