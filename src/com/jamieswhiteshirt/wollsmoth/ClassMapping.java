package com.jamieswhiteshirt.wollsmoth;

import java.util.ArrayList;
import java.util.HashMap;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

public class ClassMapping
{
	public final HashMap<MethodMapping, MethodMapping> methods = new HashMap<MethodMapping, MethodMapping>();
	public final HashMap<FieldMapping, FieldMapping> fields = new HashMap<FieldMapping, FieldMapping>();
	public final ArrayList<String> interfaces = new ArrayList<String>();
	
	public String originalName;
	public String name;
	public String superClass;
	public boolean isInterface;
	public boolean readOnly;
	
	public ClassMapping(ClassNode node, boolean readOnly)
	{
		this.originalName = node.name;
		this.name = node.name;
		this.superClass = node.superName;
		this.readOnly = readOnly;
		
		for(int i = 0; i < node.methods.size(); i++)
		{
			MethodMapping method = new MethodMapping((MethodNode)node.methods.get(i));
			this.methods.put(method, new MethodMapping(method));
		}
		
		for(int i = 0; i < node.fields.size(); i++)
		{
			FieldMapping field = new FieldMapping((FieldNode)node.fields.get(i));
			this.fields.put(field, new FieldMapping(field));
		}
		
		for(int i = 0; i < node.interfaces.size(); i++)
		{
			interfaces.add((String)node.interfaces.get(i));
		}
	}
	
	public ClassMapping getSuper()
	{
		return Mappings.getClassMapping(superClass);
	}
	
	public ArrayList<ClassMapping> getInterfaces()
	{
		ArrayList<ClassMapping> list = new ArrayList<ClassMapping>();
		for(int i = 0; i < interfaces.size(); i++)
		{
			list.add(Mappings.getClassMapping(interfaces.get(i)));
		}
		
		return list;
	}
	
	public MethodMapping getMethod(MethodMapping method)
	{
		return getMethod(method, false);
	}
	
	private MethodMapping getMethod(MethodMapping method, boolean canBeNull)
	{
		MethodMapping m = this.methods.get(method);
		if(m == null)
		{
			m = getSuperMethod(method);
		}
		if(m == null && !canBeNull) m = new MethodMapping(method);
		
		return m;
	}
	
	public MethodMapping getSuperMethod(MethodMapping method)
	{
		MethodMapping m = null;
		ClassMapping superClass = getSuper();
		if(superClass != null) m = superClass.getMethod(method, true);
		if(m == null)
		{
			ArrayList<ClassMapping> interfacesList = getInterfaces();
			for(int i = 0; i < interfacesList.size() && m == null; i++)
			{
				ClassMapping c = interfacesList.get(i);
				if(c != null) m = c.getMethod(method, true);
			}
		}
		
		return m;
	}
	
	public FieldMapping getField(FieldMapping field)
	{
		return getField(field, false);
	}
	
	private FieldMapping getField(FieldMapping field, boolean canBeNull)
	{
		FieldMapping f = this.fields.get(field);
		if(f == null)
		{
			f = getSuperField(field);
		}
		if(f == null && !canBeNull) f = new FieldMapping(field);
		
		return f;
	}
	
	public FieldMapping getSuperField(FieldMapping field)
	{
		FieldMapping f = null;
		ClassMapping superClass = getSuper();
		if(superClass != null) f = superClass.getField(field, true);
		if(f == null)
		{
			ArrayList<ClassMapping> interfacesList = getInterfaces();
			for(int i = 0; i < interfacesList.size() && f == null; i++)
			{
				ClassMapping c = interfacesList.get(i);
				if(c != null) f = c.getField(field, true);
			}
		}
		
		return f;
	}
}