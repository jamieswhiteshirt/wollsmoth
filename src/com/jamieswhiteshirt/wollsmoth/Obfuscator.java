package com.jamieswhiteshirt.wollsmoth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.LocalVariableNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeInsnNode;

public class Obfuscator
{
	private static HashSet<MethodMapping> unmodifiables = new HashSet<MethodMapping>();
	
	private static HashMap<String, ClassNode> classNodes;
	
	private static INamer namer;
	
	static
	{
		unmodifiables.add(new MethodMapping("main", "([Ljava/lang/String;)V"));
	}
	
	public static void obfuscate(HashMap<String, ClassNode> classMap, INamer in)
	{
		classNodes = classMap;
		namer = in;
		
		obfuscatePackage(Mappings.packages);
		obfuscateClassNames();
		obfuscateInterfaceMethodsAndFields(Mappings.interfaces());
		obfuscateClassMethodsAndFields(Mappings.parentlessClasses(), new HashMap<String, Integer>(), 0);
		obfuscateInstructions();
	}
	
	private static void obfuscatePackage(HashMap<String, PackageMapping> subPackages)
	{
		int index = 0;
		
		Iterator<PackageMapping> it = subPackages.values().iterator();
		while(it.hasNext())
		{
			PackageMapping subPackage = it.next();
			subPackage.name = namer.getPackageName(index++);
			
			obfuscatePackage(subPackage.children);
		}
	}
	
	private static void obfuscateClassNames()
	{
		HashMap<String, Integer> packageClassIndices = new HashMap<String, Integer>();
		HashMap<String, Integer> packageInterfaceIndices = new HashMap<String, Integer>();
		
		Iterator<ClassMapping> it = Mappings.classes.values().iterator();
		while(it.hasNext())
		{
			ClassMapping c = it.next();
			
			if(c.readOnly) continue;
			
			int lastSlash = c.name.lastIndexOf('/');
			String packageName = null;
			
			if(lastSlash == -1)
			{
				packageName = "";
			}
			else
			{
				packageName = Mappings.getPackage(c.name.substring(0, lastSlash)) + "/";
			}
			
			if(!c.isInterface)
			{
				Integer packageIndex = packageClassIndices.get(packageName);
				if(packageIndex == null)
				{
					packageIndex = 0;
				}
				
				c.name = packageName + namer.getClassName(packageIndex);
				
				packageClassIndices.put(packageName, packageIndex + 1);
			}
			else
			{
				Integer packageIndex = packageInterfaceIndices.get(packageName);
				if(packageIndex == null)
				{
					packageIndex = 0;
				}
				
				c.name = packageName + namer.getInterfaceName(packageIndex);
				
				packageInterfaceIndices.put(packageName, packageIndex + 1);
			}
		}
	}
	
	private static void obfuscateInterfaceMethodsAndFields(ArrayList<ClassMapping> interfaces)
	{
		for(int i = 0; i < interfaces.size(); i++)
		{
			ClassMapping currentInterface = interfaces.get(i);
			
			if(currentInterface.readOnly) continue;
			
			HashMap<String, Integer> methodIndices = new HashMap<String, Integer>();
			int fieldIndex = 0;
			
			Iterator<Entry<MethodMapping, MethodMapping>> methods = currentInterface.methods.entrySet().iterator();
			while(methods.hasNext())
			{
				Entry<MethodMapping, MethodMapping> method = methods.next();
				
				if(!unmodifiables.contains(method.getKey()) && !method.getKey().name.equals("<init>") && !method.getKey().name.equals("<clinit>"))
				{
					Integer methodIndex = methodIndices.get(method.getKey().desc);
					if(methodIndex == null) methodIndex = 0;
					
					method.getValue().name = namer.getInterfaceMethodName(methodIndex);
					
					methodIndices.put(method.getKey().desc, methodIndex + 1);
				}
				method.getValue().desc = Mappings.formatDescriptor(method.getValue().desc);
			}
			
			Iterator<Entry<FieldMapping, FieldMapping>> fields = currentInterface.fields.entrySet().iterator();
			while(fields.hasNext())
			{
				Entry<FieldMapping, FieldMapping> field = fields.next();
				field.getValue().name = namer.getInterfaceFieldName(fieldIndex++);
				field.getValue().desc = Mappings.formatDescriptor(field.getValue().desc);
			}
		}
	}
	
	private static void obfuscateClassMethodsAndFields(ArrayList<ClassMapping> classes, HashMap<String, Integer> methodStartIndices, int fieldStartIndex)
	{
		for(int i = 0; i < classes.size(); i++)
		{
			HashMap<String, Integer> methodIndices = new HashMap<String, Integer>(methodStartIndices);
			int fieldIndex = fieldStartIndex;
			
			ClassMapping currentClass = classes.get(i);
			
			Iterator<Entry<MethodMapping, MethodMapping>> methods = currentClass.methods.entrySet().iterator();
			while(methods.hasNext())
			{
				Entry<MethodMapping, MethodMapping> method = methods.next();
				
				MethodMapping superMethod = currentClass.getSuperMethod(method.getKey());
				
				if(superMethod == null)
				{
					if(!unmodifiables.contains(method.getKey()) && !method.getKey().name.equals("<init>") && !method.getKey().name.equals("<clinit>"))
					{
						Integer methodIndex = methodIndices.get(method.getKey().desc);
						if(methodIndex == null) methodIndex = 0;
						
						method.getValue().name = namer.getClassMethodName(methodIndex);
						
						methodIndices.put(method.getKey().desc, methodIndex + 1);
					}
					method.getValue().desc = Mappings.formatDescriptor(method.getValue().desc);
				}
				else
				{
					method.setValue(new MethodMapping(superMethod));
				}
			}
			
			Iterator<Entry<FieldMapping, FieldMapping>> fields = currentClass.fields.entrySet().iterator();
			while(fields.hasNext())
			{
				Entry<FieldMapping, FieldMapping> field = fields.next();
				field.getValue().name = namer.getClassFieldName(fieldIndex++);
				field.getValue().desc = Mappings.formatDescriptor(field.getValue().desc);
			}
			
			obfuscateClassMethodsAndFields(Mappings.directChildrenOf(currentClass.originalName), methodIndices, fieldIndex);
		}
	}
	
	private static void obfuscateInstructions()
	{
		Iterator<ClassNode> it = classNodes.values().iterator();
		while(it.hasNext())
		{
			ClassNode classNode = it.next();
			String className = classNode.name;
			
			classNode.name = Mappings.getClass(classNode.name);
			classNode.superName = Mappings.getClass(classNode.superName);
			
			for(int i = 0; i < classNode.interfaces.size(); i++)
			{
				classNode.interfaces.set(i, Mappings.getClass((String)classNode.interfaces.get(i)));
			}
			
			for(int i = 0; i < classNode.methods.size(); i++)
			{
				MethodNode method = (MethodNode)classNode.methods.get(i);
				
				MethodMapping m = Mappings.getMethod(className, method.name, method.desc);
				method.name = m.name;
				method.desc = m.desc;
				if(method.signature != null) method.signature = Mappings.formatDescriptor(method.signature);
				
				if(method.localVariables != null)
				{
					for(int j = 0; j < method.localVariables.size(); j++)
					{
						LocalVariableNode localVariable = (LocalVariableNode)method.localVariables.get(j);
						
						localVariable.name = namer.getLocalVariableName(localVariable.index);
						localVariable.desc = Mappings.formatDescriptor(localVariable.desc);
						if(localVariable.signature != null) localVariable.signature = Mappings.formatDescriptor(localVariable.signature);
					}
				}
				
				for(int j = 0; j < method.instructions.size(); j++)
				{
					AbstractInsnNode instruction = method.instructions.get(j);
					
					if(instruction instanceof MethodInsnNode)
					{
						patch((MethodInsnNode)instruction);
					}
					else if(instruction instanceof FieldInsnNode)
					{
						patch((FieldInsnNode)instruction);
					}
					else if(instruction instanceof TypeInsnNode)
					{
						patch((TypeInsnNode)instruction);
					}
				}
			}
			
			for(int i = 0; i < classNode.fields.size(); i++)
			{
				FieldNode field = (FieldNode)classNode.fields.get(i);
				
				FieldMapping f = Mappings.getField(className, field.name, field.desc);
				field.name = f.name;
				field.desc = f.desc;
				if(field.signature != null) field.signature = Mappings.formatDescriptor(field.signature);
			}
		}
	}
	
	private static void patch(MethodInsnNode insn)
	{
		MethodMapping m = Mappings.getMethod(insn.owner, insn.name, insn.desc);
		insn.owner = Mappings.getClass(insn.owner);
		insn.name = m.name;
		insn.desc = m.desc;
	}
	
	private static void patch(FieldInsnNode insn)
	{
		FieldMapping m = Mappings.getField(insn.owner, insn.name, insn.desc);
		insn.owner = Mappings.getClass(insn.owner);
		insn.name = m.name;
		insn.desc = m.desc;
	}
	
	private static void patch(TypeInsnNode insn)
	{
		insn.desc = Mappings.getClass(insn.desc);
	}
}